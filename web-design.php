<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <!-- Author -->
    <meta name="author" content="Themes Industry">
    <!-- description -->
    <meta name="description"
        content="MegaOne is a highly creative, modern, visually stunning and Bootstrap responsive multipurpose studio and portfolio HTML5 template with 8 ready home page demos.">
    <!-- keywords -->
    <meta name="keywords"
        content="Creative Startup, modern, clean, bootstrap responsive, html5, css3, portfolio, blog, studio, templates, multipurpose, one page, corporate, start-up, studio, branding, designer, freelancer, carousel, parallax, photography, studio, masonry, grid, faq">
    <!-- Page Title -->
    <title>RED PRESTIGE MEDIA - Digital Agency | Web Design Jakarta</title>

    <meta property="og:title" content="RED PRESTIGE MEDIA - Digital Agency | Web Design Jakarta" />
    <meta property="og:description"
        content="Digital marketing agency in Jakarta. One-stop digital marketing &amp; advertising solution which includes web design, web development, digital activation." />
    <meta property="og:url" content="www.redprestige.id" />
    <meta property="og:image" content="http://www.redprestige.id/include/img/share.png" />

    <!-- FAVICONS ICON -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="include/img/favicon.png" />

    <!-- Bundle -->
    <link href="vendor/css/bundle.min.css" rel="stylesheet">
    <!-- Plugin Css -->
    <link href="include/css/line-awesome.min.css" rel="stylesheet">
    <link href="vendor/css/revolution-settings.min.css" rel="stylesheet">
    <link href="vendor/css/jquery.fancybox.min.css" rel="stylesheet">
    <link href="vendor/css/owl.carousel.min.css" rel="stylesheet">
    <link href="vendor/css/cubeportfolio.min.css" rel="stylesheet">
    <link href="vendor/css/LineIcons.min.css" rel="stylesheet">
    <link href="include/css/slick.css" rel="stylesheet">
    <link href="include/css/slick-theme.css" rel="stylesheet">
    <link href="vendor/css/wow.css" rel="stylesheet">
    <!-- Style Sheet -->
    <link href="include/css/blog.css" rel="stylesheet">
    <link href="include/css/style.css?v=1" rel="stylesheet">
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="90">

    <!-- Preloader -->
    <div class="preloader">
        <div class="dot-container">
            <div class="dot dot-1"></div>
            <div class="dot dot-2"></div>
            <div class="dot dot-3"></div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
            <defs>
                <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 21 -7" />
                </filter>
            </defs>
        </svg>
    </div>
    <!-- Preloader End -->

    <!--Header Start-->
    <header id="home" class="cursor-light">

        <div class="inner-header nav-icon no-shadow">
            <!--toggle btn-->

            <a href="javascript:void(0)" class="sidemenu_btn link" id="sidemenu_toggle">
                <span></span>
                <span></span>
                <span></span>
            </a>

            <a class="navbar-brand link" href="index-include.html">
                <img src="include/img/logo-redprestige.png" class="logo" alt="logo">
            </a>
        </div>
        <!--Side Nav-->
        <div class="side-menu hidden side-menu-opacity">
            <div class="bg-overlay"></div>
            <div class="inner-wrapper">
                <span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
                <div class="container">
                    <div class="row w-100 side-menu-inner-content">
                        <div class="col-12 d-flex justify-content-center align-items-center">
                            <a href="index-include.html" class="navbar-brand"><img
                                    src="include/img/logo-redprestige.png" alt="logo"></a>
                        </div>
                        <div class="col-12 col-lg-8">
                            <nav class="side-nav w-100">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#home">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#feature-sec">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#portfolio-sec">Work</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#contact-sec">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-12 col-lg-4 d-flex align-items-center">
                            <div class="side-footer text-white w-100">
                                <div class="menu-company-details">
                                    <span>+62 851-7330-4278</span>
                                    <span><a href="mailto:sales@redprestige.id"
                                            target="_blank">sales@redprestige.id</a></span>
                                </div>
                                <ul class="social-icons-simple">
                                    <li><a class="facebook-text-hvr" href="javascript:void(0)"><i
                                                class="fab fa-facebook-f"></i> </a> </li>
                                    <li><a class="twitter-text-hvr" href="javascript:void(0)"><i
                                                class="fab fa-twitter"></i> </a> </li>
                                    <li><a class="instagram-text-hvr" href="javascript:void(0)"><i
                                                class="fab fa-instagram"></i> </a> </li>
                                </ul>
                                <p class="text-white">&copy; 2023 PT. RED PRESTIGE MEDIA</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a id="close_side_menu" href="javascript:void(0);"></a>

    </header>
    <!--Header End-->

    <section class="web-design padding-bottom">

        <div style="background-color:#2C348C;padding:100px 0">
            <p style="color:#fff;text-align:center;font-size:30px;font-weight:600">LAYANAN WEBSITE DESIGN</p>
        </div>

        <div class="container my-5">
            <div
                style="width:100%;max-width:250px;margin:0 auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:12px;font-weight:500">
                Website Design</div>
            <h3 class="text-center my-3">#1 Jasa Website Design Terbaik dan Professional</h3>
            <p style="font-size:24px;line-height:1.8">RED PRESTIGE menawarkan jasa pembuatan website profesional yang
                dapat membantu mengubah website Anda
                menjadi alat pemasaran yang kuat.
                Kami memiliki tim pengembang berpengalaman yang terampil dalam membuat website yang kreatif dan baik
                secara teknis.
                Dengan bantuan kami, Anda dapat memastikan bahwa website Anda membuat dampak yang signifikan pada target
                pasar Anda.
                Berbagai Katagori Niche seperti : Company Profile, Online Store, Blog, News, Travel, dsb.</p>
        </div>

        <div class="container my-5">
            <div
                style="width:100%;max-width:250px;margin:0 auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:12px;font-weight:500">
                Fitur</div>
            <h3 class="text-center my-3">Fitur Website Yang Akan Didapatkan</h3>
            <h5 class="text-center my-3" style="font-weight:300">*Tergantung dari paket yang dipilih</h5>
            <div class="row my-5" style="width:100%;max-width:768px;margin:0 auto;">
                <div class="col-lg-6">
                    <ul style="font-size:20px;line-height:2;font-weight:600">
                        <li>Domain Website</li>
                        <li>SSL Security</li>
                        <li>Server Hosting</li>
                        <li>Loading Cepat</li>
                        <li>Whois Domain Protected</li>
                        <li>Anti Spam Hacker</li>
                        <li>Design Modern Corporate</li>
                        <li>Responsive Semua Device</li>
                        <li>SEO Optimize</li>
                        <li>Email Bisnis</li>
                        <li>Whatsapp Chat Premium</li>
                        <li>Artikel SEO Original</li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <ul style="font-size:20px;line-height:2;font-weight:600">
                        <li>Install Google Search Console</li>
                        <li>Install Google Analytic</li>
                        <li>Install Pixel Facebook</li>
                        <li>Install Tracking GTM</li>
                        <li>Pembuatan Fanpage Facebook</li>
                        <li>Pembuatan Instagram Bisnis</li>
                        <li>Pembuatan Channel Youtube</li>
                        <li>Pembuatan Logo</li>
                        <li>Design Kartu Nama</li>
                        <li>Design Company Profile</li>
                        <li>Gratis Budget Iklan</li>
                        <li>Maintenance 1 Tahun</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container" style="width:100%;max-width:1280px;margin:20px auto;">
            <div
                style="width:100%;max-width:250px;margin:20px auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:12px;font-weight:500">
                Pilih Paket Website</div>

            <h3 class="text-center my-3">Pilih Paket Website Design</h3>
            <p style="font-size:24px;line-height:1.8;text-align:center">Harga spesial, kesempatan terbatas setiap
                bulannya!</p>

            <div class="row">

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Landing Page</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 2.199.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>8 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>1 Halaman Website</b></li>
                                <li><b>1 Email Bisnis</b></li>
                                <li><b>2 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Anti Spam Hacker</li>
                                <li>Whatsapp Chat Premium</li>
                                <li><b>1 Design Banner Website</b></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">ORDER NOW</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Regular</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 4.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>20 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>5 Halaman Website</b></li>
                                <li><b>7 Email Bisnis</b></li>
                                <li><b>3 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Anti Spam Hacker</li>
                                <li>Whatsapp Chat Premium</li>
                                <li><b>5 Design Banner Website</b></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">ORDER NOW</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Business</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 9.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>40 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>20 Halaman Website</b></li>
                                <li><b>25 Email Bisnis</b></li>
                                <li><b>7 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Anti Spam Hacker</li>
                                <li>Whatsapp Chat Premium</li>
                                <li><b>15 Design Banner Website</b></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">ORDER NOW</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="container my-5">
            <div
                style="width:100%;max-width:250px;margin:0 auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:12px;font-weight:500">
                Cara Order
            </div>
            <div class="row my-5" style="width:100%;max-width:1440px;margin:0 auto;">
                <div class="col-lg-4 card p-5 wow fadeInLeft">
                    <h3 class="text-center mb-3 text-danger">1</h3>
                    <h5>Free Konsultasi + Meeting</h5>
                    <p class="my-3" style="font-size:20px;line-height:1.8">Pada tahap ini client mengkomunikasikan
                        terkait dengan konsep website yang akan dibuat.</p>
                </div>
                <div class="col-lg-4 card p-5 wow fadeInLeft">
                    <h3 class="text-center mb-3 text-danger">2</h3>
                    <h5>MOU + Invoice</h5>
                    <p class="my-3" style="font-size:20px;line-height:1.8">Tahap selanjutnya adalah kami mengirimkan
                        MOU serta Invoice kepada client.</p>
                </div>
                <div class="col-lg-4 card p-5 wow fadeInLeft">
                    <h3 class="text-center mb-3 text-danger">3</h3>
                    <h5>Down Payment 50%</h5>
                    <p class="my-3" style="font-size:20px;line-height:1.8">Tahap terakhir client melakukan
                        pembayaran DP sejumlah 50% dari nilai total pembayaran.</p>
                </div>
            </div>
        </div>
    </section>

    <!--Contact section start-->
    <section class="contact-sec padding-top padding-bottom" id="contact-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 wow fadeInLeft">
                    <h4 class="heading text-center text-lg-left">Questions? <span>Let's Get In Touch</span></h4>
                    <form class="row contact-form" id="contact-form-data">
                        <div class="col-sm-12" id="result"></div>
                        <div class="col-12 col-md-5">
                            <input type="text" name="userName" placeholder="Your Name" class="form-control">
                            <input type="email" name="userEmail" placeholder="Email Address *" class="form-control">
                            <input type="text" name="userSubject" placeholder="Subject" class="form-control">
                        </div>
                        <div class="col-12 col-md-7">
                            <textarea class="form-control" name="userMessage" rows="6">Your Message</textarea>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0);"
                                class="btn red-btn btn-hvr btn-hvr-red rounded-pill w-100 contact_btn"><i
                                    class="fa fa-spinner fa-spin mr-2 d-none" aria-hidden="true"></i>Send Message
                                <span></span><span></span><span></span><span></span><span></span>
                            </a>
                        </div>
                    </form>
                </div>
                <div class="col-12 col-lg-5 text-center text-lg-left position-relative d-flex align-items-center">
                    <div class="contact-details wow fadeInRight">
                        <ul>
                            <li><i class="las la-map-marker addr color-blue"></i>PT. RED PRESTIGE MEDIA Jl. Raya Tapos,
                                RT 001 RW 011 Tapos, Kota Depok, Jawa Barat. 16457
                            </li>
                            <li><i class="las la-phone-volume phone color-yellow"></i>
                                <span>+62 851-7330-4278</span>
                            </li>
                            <li><i class="las la-paper-plane email color-green"></i><a
                                    href="mailto:sales@redprestige.id" target="_blank">sales@redprestige.id</a></li>
                        </ul>
                    </div>
                    <img src="include/img/contact-background.png" class="contact-background" alt="contact">
                </div>
            </div>
        </div>
    </section>
    <!--Contact section end-->

    <!--Footer Start-->
    <footer class="footer-style-1">

        <div class="container">
            <div class="row align-items-center">
                <!--Social-->
                <div class="col-12 col-lg-6 text-center text-lg-left order-2 order-lg-1">
                    <div class="footer-social text-center text-lg-left ">
                        <ul class="list-unstyled">
                            <li><a class="wow fadeInUp" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-facebook-f"></i></a></li>
                            <li><a class="wow fadeInDown" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-twitter"></i></a></li>
                            <li><a class="wow fadeInDown" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-linkedin-in"></i></a></li>
                            <li><a class="wow fadeInUp" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    <p class="company-about fadeIn mt-3 mt-lg-5">&copy; 2023 PT. RED PRESTIGE MEDIA.
                    </p>
                </div>
                <!--Text-->
                <div class="col-12 col-lg-6 text-center order-1 order-lg-2">
                    <div class="contact-pot">
                        <img src="include/img/contact-pot.png" class="wow slideInRight" data-wow-delay=".5"
                            alt="contact-pot">
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--Footer End-->

    <!--Animated Cursor-->
    <div class="aimated-cursor">
        <div class="cursor">
            <div class="cursor-loader"></div>
        </div>
    </div>
    <!--Animated Cursor End-->

    <!--Scroll Top Start-->
    <span class="scroll-top-arrow"><i class="fas fa-angle-up"></i></span>
    <!--Scroll Top End-->

    <!-- JavaScript -->
    <script src="vendor/js/bundle.min.js"></script>
    <!-- Plugin Js -->
    <script src="vendor/js/jquery.appear.js"></script>
    <script src="vendor/js/jquery.fancybox.min.js"></script>
    <script src="vendor/js/owl.carousel.min.js"></script>
    <script src="vendor/js/parallaxie.min.js"></script>
    <script src="vendor/js/wow.min.js"></script>
    <script src="vendor/js/jquery.cubeportfolio.min.js"></script>

    <!--Tilt Js-->
    <script src="vendor/js/TweenMax.min.js"></script>
    <!-- custom script-->
    <script src="include/js/circle-progress.min.js"></script>
    <script src="vendor/js/contact_us.js"></script>
    <script src="include/js/script.js"></script>
    <script src="include/js/functions.js"></script>

</body>

</html>