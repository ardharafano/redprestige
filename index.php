<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <!-- Author -->
    <meta name="author" content="Themes Industry">
    <!-- description -->
    <meta name="description"
        content="MegaOne is a highly creative, modern, visually stunning and Bootstrap responsive multipurpose studio and portfolio HTML5 template with 8 ready home page demos.">
    <!-- keywords -->
    <meta name="keywords"
        content="Creative Startup, modern, clean, bootstrap responsive, html5, css3, portfolio, blog, studio, templates, multipurpose, one page, corporate, start-up, studio, branding, designer, freelancer, carousel, parallax, photography, studio, masonry, grid, faq">
    <!-- Page Title -->
    <title>RED PRESTIGE MEDIA - Digital Agency | Web Design Jakarta</title>

    <meta property="og:title" content="RED PRESTIGE MEDIA - Digital Agency | Web Design Jakarta" />
    <meta property="og:description"
        content="Digital marketing agency in Jakarta. One-stop digital marketing &amp; advertising solution which includes web design, web development, digital activation." />
    <meta property="og:url" content="www.redprestige.id" />
    <meta property="og:image" content="http://www.redprestige.id/include/img/share.png" />

    <!-- FAVICONS ICON -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="include/img/favicon.png" />

    <!-- Bundle -->
    <link href="vendor/css/bundle.min.css" rel="stylesheet">
    <!-- Plugin Css -->
    <link href="include/css/line-awesome.min.css" rel="stylesheet">
    <link href="vendor/css/revolution-settings.min.css" rel="stylesheet">
    <link href="vendor/css/jquery.fancybox.min.css" rel="stylesheet">
    <link href="vendor/css/owl.carousel.min.css" rel="stylesheet">
    <link href="vendor/css/cubeportfolio.min.css" rel="stylesheet">
    <link href="vendor/css/LineIcons.min.css" rel="stylesheet">
    <link href="include/css/slick.css" rel="stylesheet">
    <link href="include/css/slick-theme.css" rel="stylesheet">
    <link href="vendor/css/wow.css" rel="stylesheet">
    <!-- Style Sheet -->
    <link href="include/css/blog.css" rel="stylesheet">
    <link href="include/css/style.css?v=1" rel="stylesheet">
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="90">

    <!-- Preloader -->
    <div class="preloader">
        <div class="dot-container">
            <div class="dot dot-1"></div>
            <div class="dot dot-2"></div>
            <div class="dot dot-3"></div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
            <defs>
                <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 21 -7" />
                </filter>
            </defs>
        </svg>
    </div>
    <!-- Preloader End -->

    <!--Header Start-->
    <header id="home" class="cursor-light">

        <div class="inner-header nav-icon no-shadow">
            <!--toggle btn-->

            <a href="javascript:void(0)" class="sidemenu_btn link" id="sidemenu_toggle">
                <span></span>
                <span></span>
                <span></span>
            </a>

            <a class="navbar-brand link" href="index-include.html">
                <img src="include/img/logo-redprestige.png" class="logo" alt="logo">
            </a>
        </div>
        <!--Side Nav-->
        <div class="side-menu hidden side-menu-opacity">
            <div class="bg-overlay"></div>
            <div class="inner-wrapper">
                <span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
                <div class="container">
                    <div class="row w-100 side-menu-inner-content">
                        <div class="col-12 d-flex justify-content-center align-items-center">
                            <a href="index-include.html" class="navbar-brand"><img
                                    src="include/img/logo-redprestige.png" alt="logo"></a>
                        </div>
                        <div class="col-12 col-lg-8">
                            <nav class="side-nav w-100">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#home">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#feature-sec">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#portfolio-sec">Work</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link scroll" href="#contact-sec">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-12 col-lg-4 d-flex align-items-center">
                            <div class="side-footer text-white w-100">
                                <div class="menu-company-details">
                                    <span>+62 851-7330-4278</span>
                                    <span><a href="mailto:sales@redprestige.id"
                                            target="_blank">sales@redprestige.id</a></span>
                                </div>
                                <ul class="social-icons-simple">
                                    <li><a class="facebook-text-hvr" href="javascript:void(0)"><i
                                                class="fab fa-facebook-f"></i> </a> </li>
                                    <li><a class="twitter-text-hvr" href="javascript:void(0)"><i
                                                class="fab fa-twitter"></i> </a> </li>
                                    <li><a class="instagram-text-hvr" href="javascript:void(0)"><i
                                                class="fab fa-instagram"></i> </a> </li>
                                </ul>
                                <p class="text-white">&copy; 2023 PT. RED PRESTIGE MEDIA</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a id="close_side_menu" href="javascript:void(0);"></a>

    </header>
    <!--Header End-->

    <!--Banner start-->
    <section class="banner-slider position-relative cursor-light" id="banner-slider">
        <div class="row banner-slider-row no-gutters">
            <div class="col-12 col-lg-6 bg-red banner-left-area d-flex justify-content-center align-items-center wow slideInLeft"
                data-wow-delay=".8s">
                <div class="left-banner">
                    <div class="container">
                        <div class="detail-text">
                            <div class="inner-content position-relative text-center" data-depth="0.05">
                                <h4 class="heading">Design.<span>Revolution</span></h4>
                                <div class="main-ring">
                                    <div class="slider-ring"></div>
                                </div>
                            </div>
                        </div>
                        <a href="https://vimeo.com/3024219" data-fancybox="" class="slider-play-btn link"><span><i
                                    class="las la-play"></i></span> Play Video</a>
                    </div>
                </div>
                <div class="slider-object1"><img src="include/img/slider-object1.png"></div>
                <div class="slider-object2"><img src="include/img/slider-object2.png"></div>
            </div>
            <div class="col-12 col-lg-6 banner-right-area wow slideInRight" data-wow-delay=".8s">
                <div class="row no-gutters">
                    <div class="col-12 bg-orange height-50 d-flex align-items-center slider-right-box">
                        <div class="container-fluid">
                            <div class="detail-text text-right ml-auto mr-4">
                                <span class="sub-heading">Simple & Easy</span>
                                <h4 class="heading">Digital Marketing</h4>
                                <p class="text">Melalui pemanfaatan berbagai kanal digital seperti website, mesin
                                    pencari, media sosial, email, dan iklan online, digital marketing memungkinkan
                                    bisnis untuk terhubung dengan audiens target secara efektif.</p>
                                <a class="btn white-trans-btn rounded-pill white-trans-btn-yellow-hvr">Get Started Now
                                    <span></span><span></span><span></span><span></span>
                                </a>
                            </div>
                        </div>
                        <div class="img-object wow slideInUp" data-wow-delay="1.8s">
                            <img src="include/img/slider-object3.png">
                        </div>
                    </div>
                    <div class="col-12 bg-green height-50 d-flex align-items-center slider-right-box">
                        <div class="container-fluid">
                            <div class="detail-text text-left mr-auto ml-4">
                                <span class="sub-heading">Responsive Design</span>
                                <h4 class="heading">Creative Websites</h4>
                                <p class="text">Dengan fokus pada rancangan tampilan menarik, pemrograman kustom, konten
                                    relevan, dan responsivitas optimal, website custom mampu memberikan nilai tambah
                                    signifikan. </p>
                                <a class="btn white-trans-btn rounded-pill white-trans-btn-green-hvr">Get Started Now
                                    <span></span><span></span><span></span><span></span>
                                </a>
                            </div>
                        </div>
                        <div class="img-object right wow slideInRight" data-wow-delay="1.8s">
                            <img src="include/img/slider-object4.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner End-->

    <!--Features sec start-->
    <section id="feature-sec" class="feature-sec padding-top padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-md-12 offset-md-12 col-sm-12 text-center">
                    <div class="text">
                        <div class="home-text text-black">
                            <h4 class="heading">RED Prestige Media</h4>
                            <p class="text">Kami adalah agensi digital yang berdedikasi untuk membawa visi dan impian
                                digital Anda menjadi kenyataan.
                                Layanan kami mencakup desain dan pengembangan situs web yang responsif dan menarik,
                                strategi pemasaran digital yang terukur. Kami berkomitmen untuk memberikan solusi yang
                                disesuaikan dengan kebutuhan dan anggaran Anda.</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- POP UP WEBSITE DEVELOPMENT -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Website Development</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">

                            <div class="container my-5">
                                <div
                                    style="width:100%;max-width:300px;margin:0 auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:20px;font-weight:500">
                                    Website Design & Development</div>
                                <h3 class="text-center my-3">Mengoptimalkan Kinerja Situs Web Anda</h3>
                                <p>
                                <ul>
                                    <li><strong>Desain:</strong> Fokus pada estetika visual dan pengalaman pengguna,
                                        termasuk tata letak,
                                        warna, font, dan elemen multimedia.</li>
                                    <li><strong>Pengembangan Front-end:</strong> Membangun aspek yang terlihat oleh
                                        pengguna menggunakan
                                        bahasa seperti HTML, CSS, dan JavaScript, memastikan responsif di berbagai
                                        perangkat.</li>
                                    <li><strong>Pengembangan Back-end:</strong> Mengelola operasi di sisi server,
                                        termasuk manajemen
                                        database, konfigurasi server, dan logika aplikasi dengan menggunakan bahasa
                                        seperti PHP, Python, dan
                                        Java.</li>
                                    <li><strong>Fungsionalitas:</strong> Menambahkan fitur dan interaktivitas ke
                                        website, seperti formulir,
                                        kemampuan e-commerce, dan sistem manajemen konten.</li>
                                    <li><strong>Pengujian:</strong> Pengujian yang ketat memastikan fungsionalitas,
                                        kompatibilitas, dan
                                        langkah-langkah keamanan.</li>
                                    <li><strong>Optimisasi:</strong> Meningkatkan kinerja website dalam hal kecepatan,
                                        efisiensi, dan
                                        pengalaman pengguna melalui tugas seperti kompresi gambar dan pemadatan kode.
                                    </li>
                                    <li><strong>Peluncuran:</strong> Website diterapkan ke server web dan diakses oleh
                                        pengguna di internet.
                                    </li>
                                    <li><strong>Pemeliharaan dan Pembaruan:</strong> Tugas berkelanjutan, termasuk
                                        pencadangan, pembaruan
                                        keamanan, dan manajemen konten, menjaga website tetap aman, terbaru, dan
                                        berfungsi optimal.</li>
                                </ul>
                                </p><br>
                                <p>Manfaat dari desain dan pengembangan website termasuk membangun kehadiran online,
                                    meningkatkan
                                    keterlibatan pengguna, membangun kredibilitas, memungkinkan penyesuaian dan
                                    skalabilitas, meningkatkan
                                    visibilitas mesin pencari.</p>

                            </div>

                            <div class="container my-5">
                                <div
                                    style="width:100%;max-width:150px;margin:0 auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:20px;font-weight:500">
                                    Fitur-Fitur</div>
                                <h3 class="text-center my-3">Fitur Website Yang Akan Didapatkan</h3>
                                <h5 class="text-center my-3" style="font-weight:300">*Tergantung dari paket yang dipilih
                                </h5>
                                <div class="row my-5" style="width:100%;max-width:768px;margin:0 auto;">
                                    <div class="col-lg-6">
                                        <ul style="font-size:20px;line-height:2;font-weight:600">
                                            <li>Desain dan Pengembangan Web</li>
                                            <li>Pengelolaan Domain & Hosting</li>
                                            <li>Sertifikat SSL</li>
                                            <li>Pengelolaan Konten CMS</li>
                                            <li>Analitik Web</li>
                                            <li>Keamanan dan Pengawasan Web</li>
                                            <li>Desain Responsif</li>
                                            <li>SEO Optimize</li>
                                            <li>Email Marketing</li>
                                            <li>Whatsapp Chat Premium</li>
                                            <li>Sosial Media Integration</li>
                                            <li>Original SEO Article</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">
                                        <ul style="font-size:20px;line-height:2;font-weight:600">
                                            <li>Install Google Search Console</li>
                                            <li>Install Google Analytic</li>
                                            <li>Install Google AdSense</li>
                                            <li>Install Tracking GTM</li>
                                            <li>Pembuatan Fanpage Facebook</li>
                                            <li>Pembuatan Instagram Bisnis</li>
                                            <li>Pembuatan Channel Youtube</li>
                                            <li>Pembuatan Logo</li>
                                            <li>Design Kartu Nama</li>
                                            <li>Design Company Profile</li>
                                            <li>Gratis Budget Iklan</li>
                                            <li>Maintenance 1 Tahun</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END POPUP WEBSITE DEVELOPMENT -->

            <div class="row padding-top features-cards my-5">
                <div class="col-12 col-lg-4">
                    <div class="card box text-center" data-toggle="modal" data-target="#exampleModal"
                        style="cursor:pointer">
                        <div class="feature-icon text-center">
                            <i class="lni lni-bulb"></i>
                        </div>
                        <div class="card-body">
                            <p class="card-text sub-heading text-black">
                                Website Development</p>
                            <p>Strategi Desain yang Efektif untuk Aplikasi dan Situs Web yang Menarik</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="card box text-center">
                        <div class="feature-icon text-center">
                            <i class="lni lni-briefcase color-green"></i>
                        </div>
                        <div class="card-body">
                            <p class="card-text sub-heading text-black">Software Development</p>
                            <p>Integrasi Teknologi Baru dalam Proyek Pengembangan Perangkat Lunak Anda</p>
                        </div>

                    </div>

                </div>

                <div class="col-12 col-lg-4">
                    <div class="card box text-center">
                        <div class="feature-icon text-center">
                            <i class="lni lni-heart"></i>
                        </div>
                        <div class="card-body">
                            <p class="card-text sub-heading text-black">Social Media Handling</p>
                            <p>Mengukur Pengaruh dan Membangun Komunitas di Media Sosial Anda</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row padding-top features-cards">
                <div class="col-12 col-lg-4">
                    <div class="card box text-center">
                        <div class="feature-icon text-center">
                            <i class="lni lni-bulb"></i>
                        </div>
                        <div class="card-body">
                            <p class="card-text sub-heading text-black">Brand Strategy</p>
                            <p>Membangun Identitas Visual Merek yang Kuat</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="card box text-center">
                        <div class="feature-icon text-center">
                            <i class="lni lni-briefcase color-green"></i>
                        </div>
                        <div class="card-body">
                            <p class="card-text sub-heading text-black">Video Production</p>
                            <p>Mengoptimalkan Produksi Video dengan Teknologi Terbaru</p>
                        </div>
                    </div>

                </div>

                <div class="col-12 col-lg-4">
                    <div class="card box text-center">
                        <div class="feature-icon text-center">
                            <i class="lni lni-heart"></i>
                        </div>
                        <div class="card-body">
                            <p class="card-text sub-heading text-black">SEO Website</p>
                            <p>Tingkatkan Ranking Keyword Website di Google Search</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12 padding-top-half text-center">
                    <a href="#service-sec" class="btn blue-btn btn-hvr btn-hvr-yellow rounded-pill">Start Your Project
                        <span></span><span></span><span></span><span></span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--features sec End-->

    <!--Portfolio section start-->
    <section class="portfolio-sec padding-top padding-bottom bg-light-gray" id="portfolio-sec">
        <div class="container">
            <div class="row portfolio-heading-area text-center text-lg-left">
                <div class="col-6 col-md-10 offset-md-1 col-lg-5 offset-lg-0">
                    <h4 class="heading wow fadeInLeft">Latest & <span>Creative Work</span></h4>
                </div>
                <div class="col-12 col-md-10 offset-md-1 col-lg-7 offset-lg-0">
                    <!--<p class="text wow fadeInRight">
                        Lorem ipsum is simply dummy text of the printing and typesetting. Lorem Ipsum has been the industry’s standard dummy.  Lorem Ipsum has been the industry’s standard dummy.
                    </p>-->
                </div>
            </div>
            <div class="row position-relative">
                <div class="col-12">
                    <div class="portfolio-carousel owl-carousel owl-item">
                        <div class="item">
                            <div class="portfolio-image">
                                <img src="include/img/p1.png">
                            </div>
                            <div class="row no-gutters portfolio-footer padding-top">
                                <div class="d-flex justify-content-center">
                                    <div style="text-align:left;" class="portfolio-mini-detail">
                                        <h4 class="portfolio-mini-heading">PT Dharma Samudera Fishing Industris Tbk.
                                        </h4>
                                        <p style="width:auto; color:#333333;" class="text"><a target="_blank"
                                                href="https://dsfi.id/en">www.dsfi.id</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="portfolio-image">
                                <img src="include/img/p2.png">
                            </div>
                            <div class="row no-gutters portfolio-footer padding-top">
                                <div class="d-flex justify-content-center">
                                    <div style="text-align:left;" class="portfolio-mini-detail">
                                        <h4 class="portfolio-mini-heading">Klinik Utama ACR CARE</h4>
                                        <p style="width:auto; color:#333333;" class="text"><a target="_blank"
                                                href="https://acrcare.com/">www.acrcare.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="portfolio-image">
                                <img src="include/img/p3.png">
                            </div>
                            <div class="row no-gutters portfolio-footer padding-top">
                                <div class="d-flex justify-content-center">
                                    <div style="text-align:left;" class="portfolio-mini-detail">
                                        <h4 class="portfolio-mini-heading">PT. Boston Furniture Industries Tbk.</h4>
                                        <p style="width:auto; color:#333333;" class="text"><a target="_blank"
                                                href="https://boston-industries.com/">www.boston-industries.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="portfolio-links" id="portfolio-arr-left"><i class="fas fa-angle-left"></i> </a>
                <a class="portfolio-links" id="portfolio-arr-right"><i class="fas fa-angle-right"></i> </a>
            </div>
        </div>
    </section>
    <!--Portfolio section end-->

    <!--Service section start-->
    <section class="web-design padding-bottom" id="service-sec">

        <div style="background-color:#2C348C;padding:100px 0">
            <p style="color:#fff;text-align:center;font-size:30px;font-weight:600">SERVICES</p>
        </div>

        <div class="container" style="width:100%;max-width:1280px;margin:20px auto;">
            <div
                style="width:100%;max-width:250px;margin:20px auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:12px;font-weight:500">
                Website Development</div>

            <h3 class="text-center my-3">Pilih Paket Website Design</h3>
            <p style="font-size:24px;line-height:1.8;text-align:center">Harga spesial, kesempatan terbatas setiap
                bulannya!</p>

            <div class="row my-5">

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Landing Page</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 2.199.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>8 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>1 Halaman Website</b></li>
                                <li><b>1 Email Bisnis</b></li>
                                <li><b>2 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Anti Spam Hacker</li>
                                <li>Whatsapp Chat Premium</li>
                                <li><b>1 Design Banner Website</b></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Regular</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 4.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>20 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>5 Halaman Website</b></li>
                                <li><b>7 Email Bisnis</b></li>
                                <li><b>3 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Anti Spam Hacker</li>
                                <li>Whatsapp Chat Premium</li>
                                <li><b>5 Design Banner Website</b></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Business</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 9.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>40 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>20 Halaman Website</b></li>
                                <li><b>25 Email Bisnis</b></li>
                                <li><b>7 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Anti Spam Hacker</li>
                                <li>Whatsapp Chat Premium</li>
                                <li><b>15 Design Banner Website</b></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Corporate</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 49.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>75 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>20 Halaman Website</b></li>
                                <li><b>20 Email Bisnis</b></li>
                                <li><b>5 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Sosial Media Integration</li>
                                <li>Install Google Search Console</li>
                                <li>Install Google Analytic</li>
                                <li>Install Google AdSense</li>
                                <li>Pembuatan Fanpage Facebook</li>
                                <li>Pembuatan Instagram Bisnis</li>
                                <li>Pembuatan Channel Youtube</li>
                                <li>Whatsapp Chat Premium</li>
                                <li>3 Design Banner Website</li>
                                <li><strong>Maintenance 1 Tahun</strong></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket e-Commerce</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 74.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>100 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>35 Halaman Website</b></li>
                                <li><b>25 Email Bisnis</b></li>
                                <li><b>5 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Sosial Media Integration</li>
                                <li>Install Google Search Console</li>
                                <li>Install Google Analytic</li>
                                <li>Install Google AdSense</li>
                                <li>Pembuatan Fanpage Facebook</li>
                                <li>Pembuatan Instagram Bisnis</li>
                                <li>Pembuatan Channel Youtube</li>
                                <li>Whatsapp Chat Premium</li>
                                <li>8 Design Banner Website</li>
                                <li><strong>Maintenance 1 Tahun</strong></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Media</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 99.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Akses Cpanel</li>
                                <li>Gratis Domain</li>
                                <li>Gratis SSL</li>
                                <li>Gratis SSL Security</li>
                                <li>Gratis DDoS Protection</li>
                                <li><b>150 GB Space</b></li>
                                <li>Design Website Premium</li>
                                <li>Responsive Design</li>
                                <li><b>50 Halaman Website</b></li>
                                <li><b>35 Email Bisnis</b></li>
                                <li><b>5 Kali Revisi</b></li>
                                <li>Optimasi Website</li>
                                <li>Optimasi Search Engine Goolge</li>
                                <li>Sosial Media Integration</li>
                                <li>Original SEO Article</li>
                                <li>Install Google Search Console</li>
                                <li>Install Google Analytic</li>
                                <li>Install Google AdSense</li>
                                <li>Install Tracking GTM</li>
                                <li>Pembuatan Fanpage Facebook</li>
                                <li>Pembuatan Instagram Bisnis</li>
                                <li>Pembuatan Channel Youtube</li>
                                <li><strong>Maintenance 1 Tahun</strong></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="container" style="width:100%;max-width:1280px;margin:80px auto 0 auto;"
            id="service-seo-sec">
            <div
                style="width:100%;max-width:250px;margin:20px auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:12px;font-weight:500">
                SEO Website</div>

            <h3 class="text-center my-3">Pilih Paket SEO Booster Optimize</h3>
            <p style="font-size:24px;line-height:1.8;text-align:center">Harga spesial, kesempatan terbatas setiap
                bulannya!</p>

            <div class="row my-5">

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Booster Medium</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 4.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Berlaku 1 Bulan</li>
                                <li>Small Competition</li>
                                <li>1 Keyword Utama</li>
                                <li>Maksimal 10 Keyword Turunan</li>
                                <li>10 High Quality Backlink Authority (DA 30 – 90)</li>
                                <li>75 Backlink PBN</li>
                                <li>1 Genius Cornerstone Content @4000-5000 kata</li>
                                <li>10 Content SEO Friendly @2000 kata</li>
                                <li>SEO On Page dan Off Page Optimize</li>
                                <li>Social Signal Optimize</li>
                                <li>Report SEO Bulanan</li>
                                <li>Analisa Traffic Google Analytic</li>
                                <li>Analisa Traffic Google Search Console</li>
                                <li>Rekomendasi Strategi SEO</li>
                                <li><strong>Minimal Kontrak 6 Bulan</strong></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Booster Advance</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 7.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Berlaku 1 Bulan</li>
                                <li>Medium Competition</li>
                                <li>1 Keyword Utama</li>
                                <li>Maksimal 20 Keyword Turunan</li>
                                <li>20 High Quality Backlink Authority (DA 30 – 90)</li>
                                <li>100 Backlink PBN</li>
                                <li>1 Genius Cornerstone Content @4000-5000 kata</li>
                                <li>15 Content SEO Friendly @2000 kata</li>
                                <li>SEO On Page dan Off Page Optimize</li>
                                <li>Social Signal Optimize</li>
                                <li>Report SEO Bulanan</li>
                                <li>Analisa Traffic Google Analytic</li>
                                <li>Analisa Traffic Google Search Console</li>
                                <li>Rekomendasi Strategi SEO</li>
                                <li><strong>Minimal Kontrak 6 Bulan</strong></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 wow fadeInLeft">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Paket Booster Expert</h4>
                            <h3 class="card-text text-center text-bold text-danger mb-2">Rp. 9.999.000</h3>
                            <ul class="my-3 text-bold" style="font-size:18px;line-height:2">
                                <li>Berlaku 1 Bulan</li>
                                <li>High Competition</li>
                                <li>1 Keyword Utama</li>
                                <li>Maksimal 30 Keyword Turunan</li>
                                <li>30 High Quality Backlink Authority (DA 30 – 90)</li>
                                <li>200 Backlink PBN</li>
                                <li>1 Genius Cornerstone Content @4000-5000 kata</li>
                                <li>20 Content SEO Friendly @2000 kata</li>
                                <li>SEO On Page dan Off Page Optimize</li>
                                <li>Social Signal Optimize</li>
                                <li>Report SEO Bulanan</li>
                                <li>Analisa Traffic Google Analytic</li>
                                <li>Analisa Traffic Google Search Console</li>
                                <li>Rekomendasi Strategi SEO</li>
                                <li><strong>Minimal Kontrak 6 Bulan</strong></li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="btn btn-primary" style="border-radius:12px">Chat Whatsapp</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="container my-5">
            <div
                style="width:100%;max-width:250px;margin:0 auto;background-color:#F4D004;text-align:center;padding:5px;border-radius:12px;font-weight:500">
                Cara Order
            </div>
            <div class="row my-5" style="width:100%;max-width:1440px;margin:0 auto;">
                <div class="col-lg-4 card p-5 wow fadeInLeft">
                    <h3 class="text-center mb-3 text-danger">1</h3>
                    <h5>Free Konsultasi + Meeting</h5>
                    <p class="my-3" style="font-size:20px;line-height:1.8">Pada tahap ini client mengkomunikasikan
                        terkait dengan konsep website yang akan dibuat.</p>
                </div>
                <div class="col-lg-4 card p-5 wow fadeInLeft">
                    <h3 class="text-center mb-3 text-danger">2</h3>
                    <h5>MOU + Invoice</h5>
                    <p class="my-3" style="font-size:20px;line-height:1.8">Tahap selanjutnya adalah kami mengirimkan
                        MOU serta Invoice kepada client.</p>
                </div>
                <div class="col-lg-4 card p-5 wow fadeInLeft">
                    <h3 class="text-center mb-3 text-danger">3</h3>
                    <h5>Down Payment 30%</h5>
                    <p class="my-3" style="font-size:20px;line-height:1.8">Tahap terakhir client melakukan
                        pembayaran DP sejumlah 30% dari nilai total pembayaran.</p>
                </div>
            </div>
        </div>

    </section>
    <!--Service section end-->

    <!--Blog section start-->
    <section id="blog-sec" class="blog-sec padding-top padding-bottom bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="blog-img wow fadeInLeft">
                        <img src="include/img/blog-mokeup.png">
                        <img src="include/img/blog-mokup-img.png" id="mokeup-img">
                    </div>
                </div>
                <div class="col-12 col-md-10 offset-md-1 col-lg-6 offset-lg-0 text-center text-lg-left">
                    <div class="blog-detail wow fadeInRight">
                        <h4 class="heading">Our Media</h4>
                        <p class="text"><strong>IndoParents.com</strong> The portal provides contents that are
                            especially dedicated for parenting, health and lifestyle site created in October 2022. We
                            are here to meet the needs of young parents with high mobility.</p>
                        <a class="btn blue-btn btn-hvr btn-hvr-green rounded-pill" href="https://www.indoparents.com/"
                            target="_blank">indoParents.com
                            <span></span><span></span><span></span><span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Blog section end-->

    <!--Contact section start-->
    <section class="contact-sec padding-top padding-bottom" id="contact-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 wow fadeInLeft">
                    <h4 class="heading text-center text-lg-left">Questions? <span>Let's Get In Touch</span></h4>
                    <form class="row contact-form" id="contact-form-data">
                        <div class="col-sm-12" id="result"></div>
                        <div class="col-12 col-md-5">
                            <input type="text" name="userName" placeholder="Your Name" class="form-control">
                            <input type="email" name="userEmail" placeholder="Email Address *" class="form-control">
                            <input type="text" name="userSubject" placeholder="Subject" class="form-control">
                        </div>
                        <div class="col-12 col-md-7">
                            <textarea class="form-control" name="userMessage" rows="6">Your Message</textarea>
                        </div>
                        <div class="col-12">
                            <a href="javascript:void(0);"
                                class="btn red-btn btn-hvr btn-hvr-red rounded-pill w-100 contact_btn"><i
                                    class="fa fa-spinner fa-spin mr-2 d-none" aria-hidden="true"></i>Send Message
                                <span></span><span></span><span></span><span></span><span></span>
                            </a>
                        </div>
                    </form>
                </div>
                <div class="col-12 col-lg-5 text-center text-lg-left position-relative d-flex align-items-center">
                    <div class="contact-details wow fadeInRight">
                        <ul>
                            <li><i class="las la-map-marker addr color-blue"></i>PT. RED PRESTIGE MEDIA Jl. Raya Tapos,
                                RT 001 RW 011 Tapos, Kota Depok, Jawa Barat. 16457
                            </li>
                            <li><i class="las la-phone-volume phone color-yellow"></i>
                                <span>+62 851-7330-4278</span>
                            </li>
                            <li><i class="las la-paper-plane email color-green"></i><a
                                    href="mailto:sales@redprestige.id" target="_blank">sales@redprestige.id</a></li>
                        </ul>
                    </div>
                    <img src="include/img/contact-background.png" class="contact-background" alt="contact">
                </div>
            </div>
        </div>
    </section>
    <!--Contact section end-->

    <!--Footer Start-->
    <footer class="footer-style-1">

        <div class="container">
            <div class="row align-items-center">
                <!--Social-->
                <div class="col-12 col-lg-6 text-center text-lg-left order-2 order-lg-1">
                    <div class="footer-social text-center text-lg-left ">
                        <ul class="list-unstyled">
                            <li><a class="wow fadeInUp" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-facebook-f"></i></a></li>
                            <li><a class="wow fadeInDown" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-twitter"></i></a></li>
                            <li><a class="wow fadeInDown" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-linkedin-in"></i></a></li>
                            <li><a class="wow fadeInUp" href="javascript:void(0);"><i aria-hidden="true"
                                        class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    <p class="company-about fadeIn mt-3 mt-lg-5">&copy; 2023 PT. RED PRESTIGE MEDIA.
                    </p>
                </div>
                <!--Text-->
                <div class="col-12 col-lg-6 text-center order-1 order-lg-2">
                    <div class="contact-pot">
                        <img src="include/img/contact-pot.png" class="wow slideInRight" data-wow-delay=".5"
                            alt="contact-pot">
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--Footer End-->

    <!--Animated Cursor-->
    <div class="aimated-cursor">
        <div class="cursor">
            <div class="cursor-loader"></div>
        </div>
    </div>
    <!--Animated Cursor End-->

    <!--Scroll Top Start-->
    <span class="scroll-top-arrow"><i class="fas fa-angle-up"></i></span>
    <!--Scroll Top End-->

    <!-- JavaScript -->
    <script src="vendor/js/bundle.min.js"></script>
    <!-- Plugin Js -->
    <script src="vendor/js/jquery.appear.js"></script>
    <script src="vendor/js/jquery.fancybox.min.js"></script>
    <script src="vendor/js/owl.carousel.min.js"></script>
    <script src="vendor/js/parallaxie.min.js"></script>
    <script src="vendor/js/wow.min.js"></script>
    <script src="vendor/js/jquery.cubeportfolio.min.js"></script>

    <!--Tilt Js-->
    <script src="vendor/js/TweenMax.min.js"></script>
    <!-- custom script-->
    <script src="include/js/circle-progress.min.js"></script>
    <script src="vendor/js/contact_us.js"></script>
    <script src="include/js/script.js"></script>
    <script src="include/js/functions.js"></script>

</body>

</html>